<?php

$erors_messages = array("imie"=>"","nazwisko"=>"","nazwisko_panienskie"=>"","email"=>"","kod_pocztowy"=>"", "plec"=>"");
$errors = array();

// sprawdz czy ustawione parametry
if (isset($_GET['imie']) && isset($_GET['nazwisko']) && isset($_GET['plec']) && isset($_GET['nazwisko_panienskie']) && isset($_GET['email']) && isset($_GET['kod_pocztowy'])) {
    // waliduj czy parametry formularza nie sa puste
    if (empty($_GET['imie'])) {
        array_push($errors, "Puste imie");
        $errors_messages['imie'] = "Puste imie";
    }

    if (empty($_GET['nazwisko'])) {
        array_push($errors, "Puste nazwisko");
        $errors_messages['nazwisko'] = "Puste nazwisko";
    }

    if (empty($_GET['nazwisko_panienskie'])) {
        array_push($errors, "Puste nazwisko panienskie");
        $errors_messages['nazwisko_panienskie'] = "Puste nazwisko panienskie";
    }

    if (empty($_GET['email'])) {
        array_push($errors, "Pusty email");
        $errors_messages['email'] = "Pusty email";
    } else {
        // walidacja email
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

        $emailaddress = $_GET['email'];

        if (preg_match($pattern, $emailaddress) !== 1) {
            array_push($errors, "Nieprawidłowy email");
            $errors_messages['email'] = "Nieprawidłowy email";
        }
    }

    if (empty($_GET['kod_pocztowy'])) {
        array_push($errors, "Pusty kod pocztowy");
        $errors_messages['kod_pocztowy'] = "Pusty kod pocztowy";
    } else {
        // walidacja kodu pocztowego
        $pattern = "/^([0-9]{2})(-[0-9]{3})?$/i";
        $zipcode = $_GET['kod_pocztowy'];

        if (preg_match($pattern, $zipcode) !== 1) {
            array_push($errors, "Nieprawidłowy kod pocztowy");
            $errors_messages['kod_pocztowy'] = "Nieprawidłowy kod pocztowy";
        }

    }
    $query = http_build_query($_GET);

} else {
    if (!isset($_GET['plec'])) {
        $errors_messages['plec'] = "Nie wybrano płci";
    }
    array_push($errors, "Nie wysłano wszystkich pól");
    $query = http_build_query($_GET);
}

if (!isset($_GET['id'])) {
    // jezeli brak bledow dodaj do listy zapisanej w sesji
    if (empty($errors)) {
        if (session_id() == '') {
            //session has not started
            session_start();
        }

        $pracownik = array();
        $pracownik["imie"] = $_GET["imie"];
        $pracownik["nazwisko"] = $_GET["nazwisko"];
        $pracownik["plec"] = $_GET["plec"];
        $pracownik["nazwisko_panienskie"] = $_GET["nazwisko_panienskie"];
        $pracownik["email"] = $_GET["email"];
        $pracownik["kod_pocztowy"] = $_GET["kod_pocztowy"];

        array_push($_SESSION["pracownicy"], $pracownik);

        include '../database/connect.php';
        create_schema();
        add_worker($pracownik);

        //print_r($_SESSION);
    }
//print_r($errors);

    header('Location: ../index.php?strona=2&errors=' . serialize($errors) . '&' . $query . '&errors_messages=' . serialize($errors_messages));
} else {
    if (empty($errors)) {
        echo "edit " . $_GET["id"];
        $pracownik = array();
        $pracownik["imie"] = $_GET["imie"];
        $pracownik["nazwisko"] = $_GET["nazwisko"];
        $pracownik["plec"] = $_GET["plec"];
        $pracownik["nazwisko_panienskie"] = $_GET["nazwisko_panienskie"];
        $pracownik["email"] = $_GET["email"];
        $pracownik["kod_pocztowy"] = $_GET["kod_pocztowy"];
        $pracownik["id"] = $_GET["id"];
        include '../database/connect.php';
        create_schema();
        update_worker($pracownik);
        header('Location: ../index.php?strona=5');
    } else {
        header('Location: ../index.php?strona=5&&errors=' . serialize($errors) . '&' . $query . '&errors_messages=' . serialize($errors_messages));
    }

}
?>