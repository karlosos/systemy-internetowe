
<?php

$conn = new mysqli();

function connect_db() {
    global $conn;
    $servername = "localhost";
    $username = "si";
    $password = "si";
    $dbname = "si";

// Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
    if ($conn->connect_error) {
        die("Connection failed:" . $conn->connect_error);
    }
    echo "Connected successfully";

    /* change character set to utf8 */
    if (!$conn->set_charset("utf8")) {
        printf("Error loading character set utf8: %s\n", $conn->error);
    } else {
        printf("Current character set: %s\n", $conn->character_set_name());
    }
}

function create_schema() {
    global $conn;

    $create_table_sql = "CREATE TABLE `pracownicy` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `imie` varchar(255) COLLATE utf8_polish_ci NOT NULL,
      `nazwisko` varchar(255) COLLATE utf8_polish_ci NOT NULL,
      `plec` varchar(255) COLLATE utf8_polish_ci NOT NULL,
      `nazwisko_panienskie` varchar(255) COLLATE utf8_polish_ci NOT NULL,
      `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
      `kod_pocztowy` varchar(255) COLLATE utf8_polish_ci NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";

    if ($conn->query($create_table_sql) === TRUE) {
        echo "Table pracownicy created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }


    $create_users_table_sql = "CREATE TABLE `si`.`uzytkownicy` 
( `id` INT NOT NULL AUTO_INCREMENT , `login` VARCHAR(255) NOT NULL , `haslo` VARCHAR(255) NOT NULL , 
`uprawnienia` INT NOT NULL , `imie` VARCHAR(255) NOT NULL , `nazwisko` VARCHAR(255) NOT NULL ,
 PRIMARY KEY (`id`), UNIQUE (`login`)) ENGINE = InnoDB;


";

    if ($conn->query($create_users_table_sql) === TRUE) {
        echo "Table uzytkownicy created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

}

function add_worker($pracownik) {
    global $conn;

    $sql = "INSERT INTO `pracownicy` (`id`, `imie`, `nazwisko`, `plec`, `nazwisko_panienskie`, `email`, `kod_pocztowy`) 
    VALUES (NULL, '{$pracownik['imie']}', '{$pracownik['nazwisko']}',
    '{$pracownik['plec']}', '{$pracownik['nazwisko_panienskie']}', '{$pracownik['email']}', '{$pracownik['kod_pocztowy']}');";

    echo "<br>";
    echo $sql;
    echo "<br>";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function get_workers($page_number, $search) {
    global $conn;

    $show_on_page = 10;
    $offset_start = $page_number * $show_on_page;

    $sql = "SELECT * FROM `pracownicy` ORDER BY `id` ASC LIMIT $offset_start, $show_on_page";

    if (!empty($search)) {
        $sql = "SELECT * FROM `pracownicy` WHERE `nazwisko` LIKE '%$search%' ORDER BY `id` ASC LIMIT $offset_start, $show_on_page";
    }

    $result = $conn->query($sql);

    $pracownicy = array();

    // jezeli brak bazy
    if (!is_object($result)) {
        return false;
    }

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $pracownicy[] = $row;
        }
    } else {
        //return false;
    }

    return $pracownicy;
}

function get_workers_pages_count($search) {
    global $conn;

    $show_on_page = 10;
    if ($search === "")
        $total_pages_sql = "SELECT COUNT(*) FROM pracownicy";
    else
        $total_pages_sql = "SELECT COUNT(*) FROM pracownicy WHERE `nazwisko` LIKE '%$search%'";

    $result = mysqli_query($conn,$total_pages_sql);
    $total_rows = mysqli_fetch_array($result)[0];
    $total_pages = ceil($total_rows / $show_on_page);
    return $total_pages;
}

function get_worker($id) {
    global $conn;
    $sql = "SELECT * FROM pracownicy WHERE `id` = $id";
    $result = mysqli_query($conn, $sql);
    return $result->fetch_assoc();
}

function update_worker($pracownik) {
    global $conn;

    $sql = "UPDATE `pracownicy` SET `imie`='{$pracownik['imie']}', 
`nazwisko`='{$pracownik['nazwisko']}', `plec`='{$pracownik['plec']}', `nazwisko_panienskie`='{$pracownik['nazwisko_panienskie']}', 
`email`='{$pracownik['email']}', `kod_pocztowy`='{$pracownik['kod_pocztowy']}' WHERE `id` = {$pracownik['id']}";

    echo "<br>";
    echo $sql;
    echo "<br>";

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function delete_worker($id) {
    global $conn;

    $sql = "DELETE FROM `pracownicy` WHERE `id` = $id";

    echo "<br>";
    echo $sql;
    echo "<br>";

    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function login($username, $password) {
    global $conn;

    $username = $username;
    $password = md5($password);

    $sql = "SELECT COUNT(`id`) as count FROM `uzytkownicy` WHERE `login` = '$username' AND `haslo` = '$password'";
    $result = $conn->query($sql);
    $result = $result->fetch_assoc();

    if ($result['count'] == 1) {
        echo "ok";
        return true;
    } else {
        return false;
    }
}

function get_user_info($username) {
    global $conn;

    $sql = "SELECT * FROM `uzytkownicy` WHERE `login` = '$username'";
    $result = $conn->query($sql);
    $result = $result->fetch_assoc();

    return $result;
}

function get_user_info_from_id($id) {
    global $conn;

    $sql = "SELECT * FROM `uzytkownicy` WHERE `id` = '$id'";
    $result = $conn->query($sql);
    $result = $result->fetch_assoc();

    return $result;
}

function register_user($uzytkownik) {
    global $conn;

    $password = md5($uzytkownik['haslo']);
    $sql = "INSERT INTO `uzytkownicy` (`id`, `login`, `haslo`, `imie`, `nazwisko`) 
    VALUES (NULL, '{$uzytkownik['login']}', '$password',
    '{$uzytkownik['imie']}', '{$uzytkownik['nazwisko']}');";

    if ($conn->query($sql) === TRUE) {
        //echo "New record created successfully";
    } else {
        //echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function get_privilege_level() {
    $privilege_level = 0;
    if (!isset($_SESSION['id'])) {
        return $privilege_level;
    } else {
        $user = get_user_info_from_id($_SESSION['id']);
        $privilege_level = $user['uprawnienia'];
        return $privilege_level;
    }
}

function update_account_info($uzytkownik) {
    global $conn;

    if (empty($uzytkownik['haslo'])) {
        $sql = "UPDATE `uzytkownicy` SET `login`='{$uzytkownik['login']}', `imie`='{$uzytkownik['imie']}', `nazwisko`='{$uzytkownik['nazwisko']}' WHERE `id` = {$uzytkownik['id']}";
    } else {
        $password = md5($uzytkownik['haslo']);

        $sql = "UPDATE `uzytkownicy` SET `login`='{$uzytkownik['login']}', 
`haslo`='{$password}', `imie`='{$uzytkownik['imie']}', `nazwisko`='{$uzytkownik['nazwisko']}' WHERE `id` = {$uzytkownik['id']}";

    }
    //echo "<br>";
    //echo $sql;
    //echo "<br>";

    if ($conn->query($sql) === TRUE) {
        //echo "Record updated successfully";
    } else {
        //echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function get_users($page_number) {
    global $conn;

    $show_on_page = 5;
    $offset_start = $page_number * $show_on_page;

    $sql = "SELECT `id`, `login`, `imie`, `nazwisko`, `uprawnienia` FROM `uzytkownicy` ORDER BY `id` ASC LIMIT $offset_start, $show_on_page";
    $result = $conn->query($sql);

    $users = array();

    // jezeli brak bazy
    if (!is_object($result)) {
        return false;
    }

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $users[] = $row;
        }
    } else {
        //return false;
    }

    return $users;
}

function get_users_pages_count() {
    global $conn;

    $show_on_page = 5;
    $total_pages_sql = "SELECT COUNT(*) FROM uzytkownicy";

    $result = mysqli_query($conn,$total_pages_sql);
    $total_rows = mysqli_fetch_array($result)[0];
    $total_pages = ceil($total_rows / $show_on_page);

    return $total_pages;
}

function update_privilege($id, $privilege) {
    global $conn;

    $sql = "UPDATE `uzytkownicy` SET `uprawnienia`='{$privilege}' WHERE `id` = {$id}";

    //echo "<br>";
    //echo $sql;
    //echo "<br>";

    if ($conn->query($sql) === TRUE) {
        //echo "Record updated successfully";
    } else {
        //echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

function delete_user($id) {
    global $conn;

    $sql = "DELETE FROM `uzytkownicy` WHERE `id` = $id";

    echo "<br>";
    echo $sql;
    echo "<br>";

    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

connect_db();
?>