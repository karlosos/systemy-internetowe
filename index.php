<?php
// startowanie sesji
if(session_id() == ''){
    //session has not started
    session_start();
    if (!isset($_SESSION["pracownicy"])) {
        $_SESSION["pracownicy"] = array();
    }
}

//session_destroy();

include 'database/connect.php';

connect_db();
create_schema();

print_r($_SESSION);

include 'parts/layout/header.php';

if (isset($_GET['strona'])) {
    if ($_GET['strona'] == 1) {
        if (isset($_SESSION['id'])) {
            include 'parts/widgets/greeting.php';
        } else {
            include 'parts/widgets/main.php';
        }
    } elseif ($_GET['strona'] == 2) {
        include 'parts/widgets/form.php';
    } elseif ($_GET['strona'] == 4) {
        include 'parts/widgets/session_list.php';
    } elseif ($_GET['strona'] == 3) {
        include 'parts/widgets/list.php';
    } elseif ($_GET['strona'] == 5) {
        if (isset($_GET["id"]))
            include 'parts/widgets/edit_worker_form.php';
         else
            include 'parts/widgets/list.php';
    } elseif ($_GET['strona'] == 6) {
        if (isset($_GET["id"]))
            include 'parts/widgets/delete_confirmation.php';
        else
            include 'parts/widgets/list.php';
    } elseif ($_GET['strona'] == 7) {
        include 'parts/widgets/login_form.php';
        // logowanie
    } elseif ($_GET['strona'] == 8) {
        include 'parts/widgets/register_form.php';
        // zarejestruj
    } elseif ($_GET['strona'] == 9) {
        // wyloguj
    } elseif ($_GET['strona'] == 10) {
        include 'parts/widgets/edit_account.php';
    } elseif ($_GET['strona'] == 11) {
        include 'parts/widgets/edit_privilege_levels.php';
    } elseif ($_GET['strona'] == 12) {
        include 'parts/widgets/delete_users.php';
    }


} else {
    include 'parts/widgets/main.php';
}

include 'parts/layout/footer.php';
?>

<script>
    function changePrivilege(select, id) {
        var selected = select.value;
        var form = document.getElementById("form_"+id);
        var uprawnienia = form.getElementsByClassName("uprawnienia")[0];
        uprawnienia.value = selected;
    }
</script>
