<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <title>Karol_Dzialowski</title>
</head>
<body class="fullheight">
<div class="header">
    <a href="">LOGO</a>
</div>

<div class="sidebar_left">
    <ul>
        <li onclick="window.location.href='index.php?strona=1'">Strona główna</li>
        <?php
            if (get_privilege_level() >= 1) {
                ?>
                <li onclick="window.location.href='index.php?strona=2'">Formularz</li>
                <li onclick="window.location.href='index.php?strona=4'">Zawartość sesji</li>
                <li onclick="window.location.href='index.php?strona=3'">Baza pracowników</li>
                <?php
            }
        ?>
        <?php
        if (get_privilege_level() >= 2) {
            ?>
            <li onclick="window.location.href='index.php?strona=5'">Edycja pracownika</li>
            <?php
        }
        ?>
        <?php
        if (get_privilege_level() >= 3) {
            ?>
            <li onclick="window.location.href='index.php?strona=6'">Usunięcie pracownika</li>
            <?php
        }
        ?>
    </ul>
    <br>
    <ul>
        <?php
        if (get_privilege_level() >= 1) {
            ?>
            <li onclick="window.location.href='index.php?strona=10'">Zmień dane</li>
            <?php
        }
        ?>
        <?php
        if (get_privilege_level() >= 4) {
            ?>
            <li onclick="window.location.href='index.php?strona=11'">Zmień poziom dostępu</li>
            <li onclick="window.location.href='index.php?strona=12'">Usuń użytkownika</li>
            <?php
        }
        ?>
    </ul>
</div>

<div class="main">