<?php
if (isset($_GET["id"])) {
    ?>
    Czy na pewno usunąć  <br>
    <button onclick="window.location='controller/delete_user.php?id=<?= $_GET["id"] ?>';">Tak</button>

    <?php
    $page_number = 0;
    if (isset($_GET["page_number"])) {
        $page_number = $_GET["page_number"];
    }
    ?>
    <button onclick="window.location='index.php?strona=12&page_number=<?= $page_number ?>'">Nie</button>

    <?php
} else {

    $page_number = 0;
    if (isset($_GET["page_number"])) {
        if ($_GET["page_number"] > 0)
            $page_number = $_GET["page_number"];
    }

    $users = get_users($page_number);

    ?>

    <table class="users_table">
        <thead>
        <th>Login</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Poziom</th>
        <th>Usuń</th>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user) { ?>
            <tr>
                <td><?= $user['login'] ?></td>
                <td><?= $user['imie'] ?></td>
                <td><?= $user['nazwisko'] ?></td>
                <td><?= $user['uprawnienia'] ?></td>
                <td>
                    <?php
                    if ($user['uprawnienia'] < 3) {
                        ?>
                        <a href="index.php?strona=12&id=<?= $user["id"] ?>">Usuń</a>
                        <?php
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>


    <?php
    $search = "";
    include 'users_pagination.php';
}