<?php
$uzytkownik = get_user_info_from_id($_SESSION['id']);
$id = $uzytkownik['id'];
$login = $uzytkownik['login'];
$imie = $uzytkownik['imie'];
$nazwisko = $uzytkownik['nazwisko'];

include 'edit_account_validation.php';

$login = isset($_POST['login']) && $errors_messages["login"] == "" ? $_POST['login'] : $login;
$imie = isset($_POST['imie']) && $errors_messages["imie"] == "" ? $_POST['imie'] : $imie;
$nazwisko = isset($_POST['nazwisko']) && $errors_messages["nazwisko"] == "" ? $_POST['nazwisko'] : $nazwisko;


if (empty($errors)) {
    $valid = true;
} else {
    $valid = false;
    //include 'form_errors.php';
}

if ($empty_form == true || $valid == false) {
    ?>
    <form class="main__form" action="index.php?strona=10" method="post">
        <input type="hidden" name="edit" value="true">
        <input type="hidden" name="id" value="<?= $id ?>">
        <label>Login:</label>
        <input type="text" name="login" value="<?= $login ?>">
        <div class="error"><?php echo $errors_messages["login"] ?></div>
        <label>Hasło:</label>
        <input type="password" name="haslo">
        <div class="error"><?php echo $errors_messages["haslo"] ?></div>
        <label>Powtórz hasło:</label>
        <input type="password" name="haslo2">
        <label>Imie:</label>
        <input type="text" name="imie" value="<?= $imie ?>">
        <div class="error"><?php echo $errors_messages["imie"] ?></div>
        <label>Nazwisko:</label>
        <input type="text" name="nazwisko" value="<?= $nazwisko ?>">
        <div class="error"><?php echo $errors_messages["nazwisko"] ?></div>
        <input class="main__edit_account_form__submit" type="submit" value="Potwierdź zmiany">
        <input class="main__edit_account_form__cancel" type="button" onclick="window.location='index.php?strona=1';"
               value="Odrzuć zmiany">
    </form>
    <?php
}
?>