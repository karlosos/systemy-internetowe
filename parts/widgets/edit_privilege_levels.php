<?php
if (isset($_POST["uprawnienia"]) && isset($_POST["id"])) {
    $id = $_POST["id"];
    $uprawnienia = $_POST["uprawnienia"];
    update_privilege($id, $uprawnienia);
}

$page_number = 0;
if (isset($_GET["page_number"])) {
    if ($_GET["page_number"] > 0)
        $page_number = $_GET["page_number"];
}

$users = get_users($page_number);

?>

    <table class="users_table">
        <thead>
        <th>Login</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Poziom</th>
        <th>Potwierdź</th>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user) { ?>
            <tr>
                <td><?= $user['login'] ?></td>
                <td><?= $user['imie'] ?></td>
                <td><?= $user['nazwisko'] ?></td>
                <td>
                    <?php
                    if ($user['uprawnienia'] < 4) {
                        ?>
                        <select style="width: 100%" onchange="changePrivilege(this, '<?= $user['id'] ?>')">
                            <option value="0" <?= $user['uprawnienia'] == 0 ? 'selected="selected"' : "" ?>>0</option>
                            <option value="1" <?= $user['uprawnienia'] == 1 ? 'selected="selected"' : "" ?>>1</option>
                            <option value="2" <?= $user['uprawnienia'] == 2 ? 'selected="selected"' : "" ?>>2</option>
                            <option value="3" <?= $user['uprawnienia'] == 3 ? 'selected="selected"' : "" ?>>3</option>
                            <option value="4" <?= $user['uprawnienia'] == 4 ? 'selected="selected"' : "" ?>>4</option>
                        </select>
                        <?php
                    } else {
                        echo $user["uprawnienia"];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($user['uprawnienia'] < 4) {

                        ?>
                        <form id="form_<?= $user['id'] ?>" method="post">
                            <input type="hidden" name="id" value="<?= $user['id'] ?>">
                            <input class="uprawnienia" type="hidden" name="uprawnienia">
                            <input type="submit" value="Potwierdź">
                        </form>
                        <?php
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>


<?php
$search = "";
include 'users_pagination.php';