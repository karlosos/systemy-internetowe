<?php

$valid = false;

if (isset($_GET['errors'])) {
    $errors = unserialize($_GET['errors']);
    if (empty($errors)) {
        $valid = true;
    } else {
        $valid = false;
        //include 'form_errors.php';
    }
} else {
    $valid = false;
}


$pracownik = get_worker($_GET["id"]);
$id = $pracownik["id"];
$imie = $pracownik["imie"];
$nazwisko = $pracownik["nazwisko"];
$plec = $pracownik["plec"];
$nazwisko_panienskie = $pracownik["nazwisko_panienskie"];
$email = $pracownik["email"];
$kod_pocztowy = $pracownik["kod_pocztowy"];

// pobranie wartosci bledow
$errors_messages = array("imie"=>"","nazwisko"=>"","nazwisko_panienskie"=>"","email"=>"","kod_pocztowy"=>"", "plec"=>"");
if (isset($_GET['errors_messages'])) {
    $errors_messages = unserialize($_GET['errors_messages']);
}

// tak na sztywno przypisuje, bo serialize usuwa puste stringi z listy bledow
if (isset($errors_messages["imie"])) {
    $imie_error =  $errors_messages["imie"];
} else {
    $imie_error = "";
}
if (isset($errors_messages["nazwisko"])) {
    $nazwisko_error =  $errors_messages["nazwisko"];
} else {
    $nazwisko_error = "";
}
if (isset($errors_messages["nazwisko_panienskie"])) {
    $nazwisko_panienskie_error =  $errors_messages["nazwisko_panienskie"];
} else {
    $nazwisko_panienskie_error = "";
}
if (isset($errors_messages["plec"])) {
    $plec_error =  $errors_messages["plec"];
} else {
    $plec_error = "";
}
if (isset($errors_messages["email"])) {
    $email_error =  $errors_messages["email"];
} else {
    $email_error = "";
}
if (isset($errors_messages["kod_pocztowy"])) {
    $kod_pocztowy_error =  $errors_messages["kod_pocztowy"];
} else {
    $kod_pocztowy_error = "";
}

// Jezeli w GET jest ustawione pole, i dane pole nie ma ustawionego bledu
// wtedy wyswietl takie pole w formularzu
$imie = isset($_GET["imie"]) && $imie_error == "" ? $_GET["imie"] : $imie;
$nazwisko = isset($_GET["nazwisko"]) && $nazwisko_error == "" ? $_GET["nazwisko"] : $nazwisko;
$plec = isset($_GET["plec"]) && $plec_error == "" ? $_GET["plec"] : $plec;
$nazwisko_panienskie = isset($_GET["nazwisko_panienskie"]) && $nazwisko_panienskie_error == "" ? $_GET["nazwisko_panienskie"] : $nazwisko_panienskie;
$email = isset($_GET["email"]) && $email_error == "" ? $_GET["email"] : $email;
$kod_pocztowy = isset($_GET["kod_pocztowy"]) && $kod_pocztowy_error == "" ? $_GET["kod_pocztowy"] : $kod_pocztowy;


if ($valid === false) { ?>

    <form class="main__form" action="controller/validate_form.php" method="get">
        <input type="hidden" name="id" value="<?=$id?>">
        <label>Imie:</label>
        <input type="text" name="imie" value="<?=$imie?>">
        <div class="error"><?php echo $imie_error ?></div>

        <label>Nazwisko:</label>
        <input type="text" name="nazwisko" value="<?=$nazwisko?>">
        <div class="error"><?php echo $nazwisko_error ?></div>

        <label class="main__form__plec__label">Płeć:</label>
        <div class="main__form__plec__radio">
            <input type="radio" name="plec" value="mężczyzna" <?php echo ($plec == 'mężczyzna') ?  "checked" : "" ;  ?>> Mężczyzna <br>
            <input type="radio" name="plec" value="kobieta" <?php echo ($plec == 'kobieta') ?  "checked" : "" ;  ?>> Kobieta <br>
        </div>
        <div class="error"><?php echo $plec_error ?></div>

        <label>Nazwisko panieńskie:</label>
        <input type="text" name="nazwisko_panienskie" value="<?=$nazwisko_panienskie?>">
        <div class="error"><?php echo $nazwisko_panienskie_error ?></div>

        <label>Email:</label>
        <input type="text" name="email" value="<?=$email?>">
        <div class="error"><?php echo $email_error ?></div>

        <label>Kod pocztowy:</label>
        <input type="text" name="kod_pocztowy" value="<?=$kod_pocztowy?>">
        <div class="error"><?php echo $kod_pocztowy_error ?></div>
        <input class="main__form--edit__submit" type="submit" value="Potwierdź zmiany">
        <input class="main__form--edit__cancel" type="button" value="Odrzuć zmiany" onclick="window.location='index.php?strona=5';">
    </form>

<?php } else { ?>
    Imie: <b><?=$imie?></b> <br>
    Nazwisko: <b><?=$nazwisko?></b> <br>
    Płeć: <b><?=$plec?></b> <br>
    Nazwisko panieńskie: <b><?=$nazwisko_panienskie?></b> <br>
    Email: <b><?=$email?></b> <br>
    Kod pocztowy: <b><?=$kod_pocztowy?></b> <br>
<?php } ?>
