<?php
    $page_number = 0;
    if (isset($_GET["page_number"])) {
        if ($_GET["page_number"] > 0)
            $page_number = $_GET["page_number"];
    }

    $search = "";
    if (isset($_GET["search"])) {
        $search = $_GET["search"];
    }

    $pracownicy = get_workers($page_number, $search);

    if ($pracownicy === false) {
        echo "Baza nie istnieje";
    } else {
        if (count($pracownicy) > 0 ) {
        ?>
        Lista użytkowników:
        <table class="user_list">
            <thead>
            <?php
                if ($_GET["strona"] == 5) {
                    echo "<th>Edycja</th>";
                }
                elseif ($_GET["strona"] == 6) {
                    echo "<th>Usuń</th>";
                }
            ?>
            <th>id</th>
            <th>Imie</th>
            <th>Nazwisko</th>
            <th>Płeć</th>
            <th>Nazwisko panieńskie</th>
            <th>Email</th>
            <th>Kod pocztowy</th>
            </thead>
            <tbody>
            <?php
                foreach ($pracownicy as $pracownik) { ?>
                    <tr>
                        <?php
                        $id = $pracownik["id"];
                        if ($_GET["strona"] == 5) {
                            echo "<th><a href='index.php?strona=5&id=$id'>Edycja</a></th>";
                        }
                        elseif ($_GET["strona"] == 6) {
                            echo "<th><a href='index.php?strona=6&id=$id'>Usuń</a></th>";
                        }
                        ?>
                        <td>
                            <?= $pracownik["id"] ?>
                        </td>
                        <td>
                            <?= $pracownik["imie"] ?>
                        </td>
                        <td>
                            <?= $pracownik["nazwisko"] ?>
                        </td>
                        <td>
                            <?= $pracownik["plec"] ?>
                        </td>
                        <td>
                            <?= $pracownik["nazwisko_panienskie"] ?>
                        </td>
                        <td>
                            <?= $pracownik["email"] ?>
                        </td>
                        <td>
                            <?= $pracownik["kod_pocztowy"] ?>
                        </td>
                    </tr>
                <?php }
            ?>
            </tbody>
        </table>
        <?php
            include 'pagination.php';
        } else {
            echo "Brak pozycji w tabeli.";
        }
    }
?>