<div class="main__paginator">
    <?php
    $strona = $_GET["strona"];
    if ($page_number > 0) {
        ?>
        <a href="index.php?strona=<?=$strona?>&search=<?=$search?>&page_number=<?= $page_number - 1 ?>"><-</a>
        <?php
    } else {
        echo "<-";
    }

    for ($i = 0; $i<get_workers_pages_count($search); $i++) {
        $real_page_number = intval($i)+1;
        if ($page_number == $i) {
            echo "$real_page_number";
        } else {
            echo "<a href='index.php?strona=$strona&search=$search&page_number=$i'>$real_page_number</a>";
        }
    }

    ?>

    <?php
    if ($page_number + 1 < get_workers_pages_count($search)) {
        $real_page_number = intval($i)+1;
        ?>
        <a href="index.php?strona=<?=$strona?>&search=<?=$search?>&page_number=<?= $real_page_number + 1 ?>">-></a>
        <?php
    } else {
        echo "->";
    }
    ?>
</div>