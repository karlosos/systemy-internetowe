<?php
$errors_messages = array("login" => "", "haslo" => "", "haslo2" => "", "imie" => "", "nazwisko" => "");
$errors = array();
$empty_form = false;
// sprawdz czy ustawione parametry
if (isset($_POST['imie']) && isset($_POST['nazwisko']) && isset($_POST['login']) && isset($_POST['haslo']) && isset($_POST['haslo2'])) {
    // waliduj czy parametry formularza nie sa puste
    if (empty($_POST['imie'])) {
        array_push($errors, "Puste imie");
        $errors_messages["imie"] = "Puste imie";
    }

    if (empty($_POST['nazwisko'])) {
        array_push($errors, "Puste nazwisko");
        $errors_messages["nazwisko"] = "Puste nazwisko";
    }

    if (empty($_POST['login'])) {
        array_push($errors, "Pusty login");
    } else {
        if (strlen($_POST['login']) < 6) {
            array_push($errors, "Za krótki login");
            $errors_messages["login"] = "Za krótki login";
        }
        $user_info = get_user_info($_POST['login']);
        if ($user_info) {
            array_push($errors, "Taki użytkownik już istnieje");
            $errors_messages["login"] = "Taki użytkownik już istnieje";
        }
    }

    if (empty($_POST['haslo'])) {
        array_push($errors, "Wypełnij hasło");
        $errors_messages["haslo"] = "Wypełnij hasło";
    } else if (empty($_POST['haslo2'])) {
        array_push($errors, "Wypełnij hasło");
        $errors_messages["haslo2"] = "Wypełnij hasło";
    } else {
        if ($_POST['haslo'] !== $_POST['haslo2']) {
            array_push($errors, "Wprowadzone hasła nie zgadzają się");
            $errors_messages["haslo2"] = "Wprowadzone hasła nie zgadzają się";
        }

        if (strlen($_POST['haslo']) < 6) {
            array_push($errors, "Za krótkie hasło");
            $errors_messages["haslo"] = "Za krótkie hasło";
        }
    }
    $query = http_build_query($_POST);
} else {
    $empty_form = true;
}

if (empty($errors) && $empty_form == false) {
    $user = array();
    $user['login'] = $_POST['login'];
    $user['haslo'] = $_POST['haslo'];
    $user['imie'] = $_POST['imie'];
    $user['nazwisko'] = $_POST['nazwisko'];
    // register user
    register_user($user);
    $imie = $_POST['imie'];
    $nazwisko = $_POST['nazwisko'];
    echo "Zarejestrowano użytkownika $imie $nazwisko";
}

?>