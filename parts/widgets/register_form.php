<?php
include 'register.php';

if (empty($errors)) {
    $valid = true;
} else {
    $valid = false;
    //include 'form_errors.php';
}

$login = isset($_POST['login']) && $errors_messages["login"] == "" ? $_POST['login'] : "";
$imie = isset($_POST['imie']) && $errors_messages["imie"] == "" ? $_POST['imie'] : "";
$nazwisko = isset($_POST['nazwisko']) && $errors_messages["nazwisko"] == "" ? $_POST['nazwisko'] : "";

if ($empty_form == true || $valid == false) {
?>

<form class="main__form" action="index.php?strona=8" method="post">
    <label>Login:</label>
    <input type="text" name="login" value="<?= $login ?>">
    <div class="error"><?php echo $errors_messages["login"] ?></div>
    <label>Hasło:</label>
    <input type="password" name="haslo">
    <div class="error"><?php echo $errors_messages["haslo"] ?></div>
    <label>Powtórz hasło:</label>
    <input type="password" name="haslo2">
    <div class="error"><?php echo $errors_messages["haslo2"] ?></div>
    <label>Imie:</label>
    <input type="text" name="imie" value="<?= $imie ?>">
    <div class="error"><?php echo $errors_messages["imie"] ?></div>
    <label>Nazwisko:</label>
    <input type="text" name="nazwisko" value="<?= $nazwisko ?>">
    <div class="error"><?php echo $errors_messages["nazwisko"] ?></div>
    <input class="main__register_form__submit" type="submit" value="Zarejestruj">
</form>

<?php }
?>