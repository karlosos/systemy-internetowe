<?php
$page_number = 0;
if (isset($_GET["page_number"])) {
    if ($_GET["page_number"] > 0)
        $page_number = $_GET["page_number"];
}

$search = "";
if (isset($_GET["search"])) {
    $search = $_GET["search"];
}

$pracownicy = get_workers($page_number, $search);

if ($pracownicy === false) {
    echo "Baza nie istnieje";
} else {
    if (count($pracownicy) > 0 ) {
        ?>
        Lista użytkowników:
        <table class="user_list">
            <thead>
            <th>id</th>
            <th>Imie</th>
            <th>Nazwisko</th>
            <th>Płeć</th>
            <th>Nazwisko panieńskie</th>
            <th>Email</th>
            <th>Kod pocztowy</th>
            </thead>
            <tbody>
            <?php
            foreach ($pracownicy as $pracownik) { ?>
                <tr>
                    <td>
                        <?= $pracownik["id"] ?>
                    </td>
                    <td>
                        <?= $pracownik["imie"] ?>
                    </td>
                    <td>
                        <?= $pracownik["nazwisko"] ?>
                    </td>
                    <td>
                        <?= $pracownik["plec"] ?>
                    </td>
                    <td>
                        <?= $pracownik["nazwisko_panienskie"] ?>
                    </td>
                    <td>
                        <?= $pracownik["email"] ?>
                    </td>
                    <td>
                        <?= $pracownik["kod_pocztowy"] ?>
                    </td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>

        <div class="main__paginator">
            <?php
            if ($page_number > 0) {
                ?>
                <a href="index.php?strona=3&page_number=<?= $page_number - 1 ?>">Poprzednia</a>
                <?php
            }
            ?>
            <a href="index.php?strona=3&page_number=<?= $page_number ?>"><?= $page_number ?></a>
            <?php
            if ($page_number + 1 < get_workers_pages_count()) {
                ?>
                <a href="index.php?strona=3&page_number=<?= $page_number + 1 ?>">Nastepna</a>
                <?php
            }
            ?>
        </div>

        <?php
    } else {
        echo "Brak pozycji w tabeli.";
    }
}
?>