Lista użytkowników

<table class="user_list">
    <thead>
    <th>Imie</th>
    <th>Nazwisko</th>
    <th>Płeć</th>
    <th>Nazwisko panieńskie</th>
    <th>Email</th>
    <th>Kod pocztowy</th>
    </thead>
    <tbody>
    <?php foreach ($_SESSION["pracownicy"] as $pracownik) { ?>
        <tr>
            <td>
                <?=$pracownik["imie"]?>
            </td>
            <td>
                <?=$pracownik["nazwisko"]?>
            </td>
            <td>
                <?=$pracownik["plec"]?>
            </td>
            <td>
                <?=$pracownik["nazwisko_panienskie"]?>
            </td>
            <td>
                <?=$pracownik["email"]?>
            </td>
            <td>
                <?=$pracownik["kod_pocztowy"]?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
