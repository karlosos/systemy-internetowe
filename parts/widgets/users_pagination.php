<div class="main__paginator">
    <?php
    $strona = $_GET["strona"];
    if ($page_number > 0) {
        ?>
        <a href="index.php?strona=<?=$strona?>&page_number=<?= $page_number - 1 ?>"><-</a>
        <?php
    } else {
        echo "<- ";
    }

    for ($i = 0; $i<get_users_pages_count(); $i++) {
        $real_page_number = intval($i);
        if ($page_number == $i) {
            echo "$real_page_number";
        } else {
            echo "<a href='index.php?strona=$strona&page_number=$i'>$real_page_number</a>";
        }
    }

    ?>

    <?php
    if ($page_number + 1 < get_users_pages_count()) {
        $real_page_number = intval($i);
        ?>
        <a href="index.php?strona=<?=$strona?>&page_number=<?= $real_page_number + 1 ?>">-></a>
        <?php
    } else {
        echo "->";
    }
    ?>
</div>